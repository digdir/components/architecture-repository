:lang: no
:wysiwig_editing: 1
ifeval::["{lang}" == "no"]
:doctitle: Om referansearkitektur for datautveksling
:keywords:
endif::[]

ifeval::["{lang}" == "en"]
:doctitle: Reference architecture for data exchange
:keywords:
endif::[]

include::../partials/module-includes.adoc[]


ifeval::["{lang}" == "no"]

[.lead]
*Referansearkitektur for datautveksling dokumenterer alternative mønstre og god praksis for utvikling, drift og forvaltning av løsninger for datautveksling. Det skilles mellom  konseptuelle beskrivelser og anvendelser med spesifikke løsningskomponenter.*

//== Omfang og avgrensing for referansearkitekturen

Datautveksling kan brytes ned i et sett av avgrensede områder og kapabiliteter. På øverste nivå skilles det mellom:


* Deling av data på forespørsel: Innhenting av data ved forespørsel til en eller flere datakilder.

* Deling av data ved forsendelse: Forsendelse av data fra en avsender til en eller flere mottakere.

* Deling av data ved publisering: Publisering av data eller hendelsesdata (notifikasjoner, med løs kopling mellom datatilbyder og datakonsument.  

Dette er illustrert i følgende figur.

.Områder innen datautveksling som dekkes av referansearkitekturen
image::Områder innen datautveksling.png[alt=Områder innen datautveksling image]


Det skille videre mellom operative og forvaltningsmessige kapabiliteter, dvs. selve datautvekslingen og det som går på tilrettelegging og vedlikehold.

Følgende figur viser en oversikt over de viktigste temaene og kapabilitetene inndelt i operativt og forvaltningsmessig og om de tilhører datatilbyder eller datakonsument. Nederst i figuren er også listet opp tverrgeånde temaer som vurderes som viktige inn i datautveksling og må ivaretas på tvers av roller og kapabiliteter.

//Videre nedbryting i detaljerte kapabiliteter og mønstre gjøres i tilknytning til beskrivelsene av referansearkitekturer for datautveksling.

// Her følger en overordnet oversikt over overordnede kapabiliteter og tverrgående temaer som sorterer under området datautveksling. 

//NOTE: Noen av de temaene som nevnes her er også aktuelle i andre sammenhenger, og beskrives ved behov i ulike kontekster.   



.Datautveksling - overordnet oversikt over kapabiliteter og temaer 
image::Datautveksling - overordnet oversikt over kapabiliteter og temaer .png[alt=Datautveksling - overordnet oversikt over kapabiliteter og temaer  image]

De tverrgående temaene beskriver områder som må ivaretas for å utveksle data i tråd med regelverk og god praksis. Dette er ikke en uttømmende liste, men angir det som anses som viktigst å beskrive.

Noen av disse temaene er også aktuelle i andre sammenhenger enn datautveksling, og vil beskrives under andre overskrifter.

Kort introduksjon ev hvert enkelt av disse temaene gis her.

. *Standarder og spesifikasjoner*
+
Standarder og spesifikasjoner (og andre føringer) kan finnes på alle lagene i _EIF-modellen_ (juridisk, organisatorisk, semantisk, teknisk). Dokumentasjon av slike føringer kan gjøres for større eller mindre områder. I gjeldende link:nab_referanse_maler_asciidoc-book-sat[mal for referansearkitekturene] omtales dette som  _interoperability specifications_.

. *Avtaleforvaltning*
+
Dette temaområdet omhandler oppsett og forvaltning av avtaler melom partene, enten bilateralt eller innen et felleskap (community).


. *Roller*
Dette temaområdet definerer xref:architecture-repository:data-exchange-roles:data-exchange-roles.adoc[Roller involvert i datautveksling].


. *Informasjonssikkerhet, tillit og personvern*
Dette temaområdet er omfattende. Det omhandler temaer som:

* Integritet, konfidensialitet, tilgjengelighet
* Tillitskjeder
* Innsyn og transparens
* Personvern
* M.v

. *Feilhåndtering ved datautveksling*
Temaområdet _feilhåndtering ved datautveksling_ omhandler hvordan feil og avvik håndteres. I tillegg til generelle prinsipper og krav,  dokumenteres særskilte aspekter i tilknytning til de ulike arkitekturmønstrene.  


. *Sporing*
+
Temaområdet _sporing_ omfatter:
+
* Sporing av dataaksess (stikkord: _informasjonssikkerhet og tillit_) 
* Sporing av datakilder (stikkord: _masterdata management_)
* Dokumentasjon av etterlevelse av lover og regler, samt andre føringer (f.eks. obligatoriske arkitekturprinsipper) 
* Dokumentasjon tilknyttet vedtak (stikkord: _innebygget arkiv_) 





////
= Brukstilfeller
== Generelt
== Deling av data på forespørsel
Innhenting av data ved forespørsel til en eller flere datakilder.
== Deling av data ved forsendelse
Forsendelse av data fra en avsender til en eller flere kjente mottakere.

== Deling av data ved publisering
Publisering av data eller hendelser med løs kopling mellom avsendere og mottakere. 
////
    

//== Aktuelle arkitekturprinsipper
//NOTE: Prinsipper for datautveksling vil bygge på felles  nasjonale arkitekturprinsipper, som er under revidering høsten 2019




// endif :lang: no
endif::[]



ifeval::["{lang}" == "en"]


//endif lang: en
endif::[]
    