:lang: no
include::architecture-repository:common:partial$commonincludes.adoc[]

// :lang: no ------------>
ifeval::["{lang}" == "no"]

* xref:index.adoc[Om Nasjonalt arkitekturverksted]

* Praktiske tips om bruk av arkitekturverkstedet
** xref:architecture-repository:reading-guide:module-reading-guide.adoc[Leseveiledning]
** Praktiske tips om bruk av git-verktøy
** xref:architecture-repository:antora:module-antora.adoc[Praktiske tips om innholdsproduksjon og publisering med Antora og Asciidoc]
** xref:architecture-repository:archi:module-archi.adoc[Praktiske tips om arkitekturmodellering og samarbeid i Archi]
** xref:architecture-repository:archi:module-archi.adoc[Praktiske tips om utveksling av arkitekturmodeller]
** Hvordan gi kommentarer og få svar på spørsmål


* xref:architecture-repository:about-topic-areas:about-topic-areas.adoc[Temaområder]

** xref:architecture-repository:about-topic-areas:about-topic-areas.adoc[Om temaområder]

** xref:architecture-repository:data-sharing:data-sharing.adoc[Datadeling]
*** xref:architecture-repository:data-sharing:data-sharing.adoc[Om temaområdet _datadeling_]
*** xref:drafts:data-sharing:data-sharing-value-streams.adoc[Referansemodeller for datadeling]


** xref:architecture-repository:about-data-exchange:about-data-exchange.adoc[Datautveksling]
*** xref:architecture-repository:about-data-exchange:about-data-exchange.adoc[Om temaområdet _datautveksling_]

*** xref:architecture-repository:data-exchange-ra:book-data-exchange-ra.adoc[Referansearkitektur for datautveksling]

**** xref:architecture-repository:data-exchange-ra:about-data-exchange-ra.adoc[Om referansearkitektur for datautveksling]


**** xref:architecture-repository:data-for-data-exchange:data-for-data-exchange.adoc[Begrepsapparat for data (datautveksling)]


**** xref:architecture-repository:data-exchange-roles:data-exchange-roles.adoc[Roller involvert i datautveksling]


**** xref:architecture-repository:data-exchange-patterns:about-data-exchange-patterns.adoc[Arkitekturmønstre for datautveksling]

***** xref:architecture-repository:data-exchange-patterns:about-data-exchange-patterns.adoc[Om arkitekturmønstre for datautveksling]

***** Basismønstre for datautveksling

****** xref:architecture-repository:one-way-push:book-one-way-push.adoc[Forsendelse (eMelding)]



****** xref:architecture-repository:request-reply:book-request-reply.adoc[Forespørsel-svar (eOppslag)]



****** xref:architecture-repository:publish-subscribe:book-publish-subscribe.adoc[Publisering-konsumering (eNotifikasjon)]


////
** Informasjonssikkerhet, tillit og personvern

** Integrerte prosesser
*** Sporing av datakilder (dataproveniens)
*** Datalagring og arkivering
*** Dokumentasjon av etterlevelse

** Sammenhengende tjenester
*** Kun en gang

** Innhenting og preparering av data for analyse
*** Overordnet verdistrøm


** Andre temaområder
*** Arkitekturparadigmer
*** Teknologier
*** Nasjonal porteføljestyring
////


////
**** xref:architecture-repository:data-discovery:data-discovery.adoc[Dataoppdagelse]

**** xref:architecture-repository:master-data-management:master-data-management.adoc[Masterdatahåndtering]
**** xref:architecture-repository:information-arcitecture:information-arcitecture.adoc[Informasjonsarkitektur]
**** xref:architecture-repository:information-security-and-trust:information-security-and-trust.adoc[Informasjonssikkerhet, tillit og personvern]
**** xref:architecture-repository:tracking:tracking.adoc[Sporing]
**** xref:architecture-repository:preparation-for-data-analysis:preparation-for-data-analysis.adoc[Innhenting og preparering av data for analyse]
**** xref:architecture-repository:data-storage-and-archiving:data-storage-and-archiving.adoc[Datalagring og arkivering]
**** xref:architecture-repository:data-sharing-infrastructure:data-sharing-infrastructure.adoc[Fellesløsninger for datadeling]
**** xref:architecture-repository:data-sharing-standards-and-specifications:data-sharing-standards-and-specifications.adoc[Standarder og spesifikasjoner for datadeling]
**** xref:architecture-repository:data-sharing-agreement-management:data-sharing-agreement-management.adoc[Avtalehåndtering for datadeling]
**** xref:architecture-repository:data-sharing-roles-and-responsibilities:data-sharing-roles-and-responsibilities.adoc[Roller og ansvar for datadeling
]
**** xref:architecture-repository:data-virtualization:data-virtualization.adoc[Datavirtualisering]
**** xref:architecture-repository:data-sharing-compliance:data-sharing-compliance.adoc[Etterlevelse av lover og regler for datadeling]
**** xref:architecture-repository:data-quality:data-quality.adoc[Datakvalitet]
**** xref:architecture-repository:digital-ready-legislation:digital-ready-legislation.adoc[Digitaliseringsvennlig regelverk]
**** xref:architecture-repository:incentive-management-for-data-sharing:incentive-management-for-data-sharing.adoc[Intensivordninger for datadeling]
////
//**** xref:architecture-repository:conformance-testing:conformance-testing.adoc[Konformitetstesting]




////
** Kapabilitetsarkitekturer
*** xref:architecture-repository:capability-architecture-1:capability-architecture-1.adoc[Kapabilitetsarkitektur 1]
////


// Products and services
// verktøykasse??
* Løsningsbyggeklosser for bruk og gjenbruk
** Fellesløsninger
** Tekniske tjenester og API-er
** Sluttbrukertjenester


// include::nav-subject-areas.adoc[]



// dsad


//*** xref:architecture-repository:enterprise-architecture:enterprise-architecture.adoc[Virksomhetsarkitektur]

//*** xref:architecture-repository:information-management:information-management.adoc[Informasjonforvaltning]





// Guidance - Good practice, recommendations, guidelines, standards, regulations
// Veiledning - god praksis, anbefalinger, retningslinjer, standarder og regler


//* Føringer og standarder



////
*** Strategiske føringer
**** Hovedsatsingsområder i regjeringens digitaliseringsstrategi
***** Sammenhengende tjenester
***** Samordning på tvers
***** Deling av data
***** Nasjonale fellesløsninger
***** Samarbeid med privat sektor
***** Digitaliseringsvennlig regelverk

**** Andre strategiske satsingsområder
***** Dataanalyse og kunstig intelligens
***** Datadrevet økonomi
***** [red]#Integrasjon med EU#

*** Styringsmessige føringer
**** Digitaliseringsrundskrivet
**** Nasjonale arkitekturprinsipper


*** Juridiske føringer
**** Oversikt over sentrale lover og regler
// Internasjonale reguleringer
// Forvaltningsloven osv.
// Se også Lover og regler under hvert temaområde


*** Organisatoriske føringer
// Stat vs. kommune, sektorprinsippet


*** Semantiske føringer
**** xref:architecture-repository:root-begrepsbeskrivelser:about-forvaltningsstandard-begrep.adoc[Begrepsstandarder]
***** xref:architecture-repository:forvaltningsstandard-begrepsbeskrivelser:main.adoc[Forvaltningsstandard begrepsbeskrivelser]
***** xref:architecture-repository:veileder-for-begrepsbeskrivelser:main.adoc[Veileder for begrepsbeskrivelser]

*** Tekniske føringer
**** Referansekatalogen
////

////
** Temaområder og kapabilitetskart
** Teknologier
** Referansemodeller
////




// Governance
* Styring og organisering

** Initiativer

*** xref:drafts:proposed-reference-architectures:proposed-reference-architecture-work.adoc[Innspill til prioritering av videre arbeid med referansearkitekturer]
**** xref:drafts:proposed-reference-architectures:følgebrev.adoc[Følgebrev]
**** xref:drafts:proposed-reference-architectures:situasjonsrapport.adoc[Situasjonsrapport]
**** xref:drafts:proposed-reference-architectures:strukturering-av-spørreskjema.adoc[Strukturering av spørreskjema]
**** xref:drafts:proposed-reference-architectures:forklaringer-til-spørsmålene.adoc[Forklaringer til spørsmålene]


////
** xref:drafts:proposed-reference-architectures:proposed-reference-architecture-work.adoc[Innspill til prioritering av videre arbeid med referansearkitekturer]
*** xref:drafts:proposed-reference-architectures:følgebrev.adoc[Følgebrev]
*** xref:drafts:proposed-reference-architectures:situasjonsrapport.adoc[Situasjonsrapport]
*** xref:drafts:proposed-reference-architectures:strukturering-av-spørreskjema.adoc[Strukturering av spørreskjema]
*** xref:drafts:proposed-reference-architectures:forklaringer-til-spørsmålene.adoc[Forklaringer til spørsmålene]
////

//** Om styring og organisering
////
** Strategiske føringer
*** Lover og regler

*** Strategiske satsingsområder
**** Hovedsatsingsområder i regjeringens digitaliseringsstrategi
***** Sammenhengende tjenester
***** Samordning på tvers
***** Deling av data
***** Nasjonale fellesløsninger
***** Samarbeid med privat sektor
***** Digitaliseringsvennlig regelverk
**** Andre satsingsområder
***** Dataanalyse og kunstig intelligens
***** Datadrevet økonomi
***** [red]#Integrasjon med EU#
////


// Methodology
* xref:architecture-repository:methods:methods.adoc[Metodikk for utvikling av nasjonal arkitektur]
** xref:architecture-repository:methods:reference-architecture-methods.adoc[Metodikk for utvikling av referansearkitekturer]



////

** Om metodikk
//Rammeverk

** Juridisk - utvikling av lovverket
// Digitaliseringsvennlig regelverk

** Organisatorisk
*** Roller og ansvar

** Teknisk og semantisk
** Arkitekturparadigmer

** Organisering
*** Om organisering
*** Roller og ansvar
// Eks. domeneansvar for data
*** Prosesser
// Eks. Styringsprosess for å prioritere behov... eller under metodikk?
*** Aktører i nasjonal arkitektur
**** Departementer
**** Digitaliseringsdirektoratet
**** KS
**** eHelse
**** UNIT

** Porteføljestyring
*** Behov
*** Utviklingsaktiviteter

** Monitorering
*** Beslutningslogg
*** Etterlevelse av lover og regler
*** Etterlevelse av føringer
*** Gevinstrealisering
*** Utbredelse
////


* Se også
** xref:drafts:ROOT:index.adoc[Utkast og innspill]
** xref:knowledge:ROOT:index.adoc[Arkitekturfaglig kunnskapsverksted]


endif::[]
// :lang: no <-----------



// :lang: en ------------>
ifeval::["{lang}" == "en"]

* xref:index.adoc[About the Norwegian National Architecture Repository]

** xref:index.adoc[Welcome]

endif::[]
// :lang: en <-----------



//*** Nasjonale målbilder
//**** Målbilde for "datafordeling"
//**** Målbilde for sammenhengende tjenester
//*** Nasjonale veikart