//module-includes
:wysiwig_editing: 1

include::editing-mode.adoc[]

ifeval::[{wysiwig_editing} == 0]
:modulepath: architecture-repository:archi-scripts:
:imagepath: {modulepath}
include::architecture-repository:common:partial$commonincludes.adoc[]
endif::[]
